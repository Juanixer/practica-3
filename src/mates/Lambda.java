/*
Copyright [2022] [Juan García-Obregón]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at:

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,2 software 
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions
and limitations under the License.
*/

package mates;

import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Lambda {
    public static double generarNumeroPiIterativo(long pasos) {
        Predicate<Double> dentro = x -> x <= 1;

        BinaryOperator<Double> generarPi = (x, y) -> (4*x)/y;
        Function<Long, Double> generarD = w -> { double resultado = 0.00; for(long i = 1; i <= w; i++){

                Supplier<Double> x = Math::random;  Supplier<Double> y = Math::random;
                if(dentro.test(x.get() * x.get() + y.get() * y.get())){
                    resultado++;
                }
            }
            return resultado;   };

        double d = generarD.apply(pasos);
        return generarPi.apply(d, (double)pasos);
    }
}
