#  Práctica 3. Obtención de una aproximación al número pi mediante expresiones lambda.


# README #

_Este readme sirve como explicación de que he escrito en el código y cual es el objetivo del mismo._

## Resumen ##

_Con este programa es posible conseguir una aproximación al numero Pi mediante las expresiones lambda estudiadas en clase utilizando el metodo Montecarlo_ 


## ¿En que consiste el Método Montecarlo? 🍭
_El uso del método de Monte Carlo para aproximar el valor de Pi consiste en dibujar un cuadrado, y dentro de ese cuadrado, dibujar un
círculo con diámetro de igual medida que uno de los lados del cuadrado._

_Luego se dibujan puntos de manera aleatoria sobre la superficie dibujada. Los puntos que están fuera del círculo y los que están dentro,
sirven como estimadores de las áreas internas y externas del círculo._


## Requisitos 📋

_Que programas y versiones deberá disponer para ejecutar el programa_

_Lenguaje de programación utilizado:_
```
java version "11.0.12"
```
_Editor de código:_

```
git version 2.33
```

## Comandos para ejecutar el programa ⚙️

_En primer lugar se deberá crear el archivo jar mediante el siguiente comando que nos proporciona nuestro makefile:_

```
make jar
```

_Una vez creado el archivo jar llamado "calculo.jar" se debe ejecutar el programa con el siguiente comando añadiendole el numero de lanzamientos a gusto propio que se quierean realizar hay que tener en cuenta lo grande que es dicho numero ya cuanto mas grande sea ese numero mas se aproximará a pi, debido a que habrá muchos mas numeros que puedan caer de manera aleatoria dentro del círculo._

```
java -jar calculo.jar *numero de puntos que se quiera ejecutar*
```



---
## Autor del código:✒️


* **Juan García-Obregón** 


## Licencia 📄

Este proyecto está bajo la Licencia Apache License, version 2.0 (The "License")

![](https://www.deividart.com/blog/wp-content/uploads/2020/05/creative-commons-by.jpg)

